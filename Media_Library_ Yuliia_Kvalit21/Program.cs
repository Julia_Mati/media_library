﻿using System;

namespace Media_Library__Yuliia_Kvalit21
{
    class Program
    {
        static void Main(string[] args)
        {
            ListFilm listLibrary = new ListFilm();
            Console.WriteLine("Welcome to our Media library! Choose the film!");
            Console.WriteLine("If you want to sort media library of films by year  - press [Y]");
            Console.WriteLine("If you want to sort media library of films by name  - press [N]");
            Console.WriteLine("If you want to sort media library of films by genre - press [G]");
            Console.WriteLine("Show the full list  media library of films  -  press [F]");
            string answer = Console.ReadLine();
            while (true)
            {
                if (answer == "Y" || answer == "y")
                {
                    listLibrary.SortByYear();
                    listLibrary.PrintList();

                    break;
                }
                else if (answer == "N" || answer == "n")
                {
                    listLibrary.SortByName();
                    listLibrary.PrintList();
                    break;
                }
                else if (answer == "G" || answer == "g")
                {
                    listLibrary.SortByGenre();
                    listLibrary.PrintList();
                    break;
                }
                else if (answer == "F" || answer == "f")
                {
                    listLibrary.PrintList();
                    break;
                }
                else
                {
                    Console.WriteLine("Oops! Incorrect input! Please choose again!");
                    answer = Console.ReadLine();
                }
            }
        }
    }
}
