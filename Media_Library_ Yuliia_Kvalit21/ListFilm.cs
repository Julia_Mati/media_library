﻿using System.Collections.Generic;

namespace Media_Library__Yuliia_Kvalit21
{
    class ListFilm
    {

        List<Film> myList = new List<Film>();
        public ListFilm()
        {
            this.AddFilm("The Circus", 1928, "comedy");
            this.AddFilm("Ace Ventura: Pet Detective", 1994, "comedy");
            this.AddFilm("Don't Be a Menace to South Central While Drinking Your Juice in the Hood", 1996, "comedy");
            this.AddFilm("Austin Powers: The Spy Who Shagged Me", 1999, "comedy");
            this.AddFilm("The Grand Budapest Hotel ", 2014, "comedy");

            this.AddFilm("Gone with the Wind", 1939, "drama");
            this.AddFilm("Green mile", 1999, "drama");
            this.AddFilm("Gladiator", 2000, "drama");
            this.AddFilm("The Pianist", 2002, "drama");
            this.AddFilm("Django Unchained", 2012, "drama");

            this.AddFilm("Speed", 1994, "action");
            this.AddFilm("The Matrix", 1999, "action");
            this.AddFilm("Casino Royale", 2006, "action");
            this.AddFilm("Fast Five", 2011, "action");
            this.AddFilm("Avengers: Endgame", 2019, "action");
        }
        public void SortByName() // This method treats null as the lesser of two values.
        {
            myList.Sort(delegate (Film x, Film y)
            {
                if (x.name == null && y.name == null) return 0;
                else if (x.name == null) return -1;
                else if (y.name == null) return 1;
                else return x.name.CompareTo(y.name);
            });
        }
        public void SortByGenre()
        {
            myList.Sort(delegate (Film x, Film y)
            {
                if (x.genre == null && y.genre == null) return 0;
                else if (x.genre == null) return -1;
                else if (y.genre == null) return 1;
                else return x.genre.CompareTo(y.genre);
            });
        }
        public void SortByYear()
        {
            myList.Sort(delegate (Film x, Film y)
            {
                return x.year.CompareTo(y.year);
            });
        }

        public void AddFilm(string name, int year, string genre)
        {
            myList.Add(new Film(name, year, genre));
        }
        public void PrintList()
        {
            foreach (Film film in myList)
            {
                film.GetInfo();
            }
        }
    }
}
