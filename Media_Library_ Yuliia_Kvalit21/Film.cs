﻿using System;

namespace Media_Library__Yuliia_Kvalit21
{
    class Film
    {
        public string name;

        public int year;

        public string genre;
        public Film(string n, int y, string g)
        {
            name = n;
            year = y;
            genre = g;
        }
        public void GetInfo()
        {
            Console.WriteLine($"Name: {name,-73}  Year: {year,4}  Genre: {genre,-10}"); //för att få mer snyggare output på skärmen använde jag interpolärad sträng
        }
    }
}
